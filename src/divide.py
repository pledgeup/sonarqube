def divide_by_zero():
    x = 5
    y = 0
    result = x / y 
    return result

def division(dividende, diviseur):
    if diviseur == 0:
        raise ZeroDivisionError("Division par zéro est interdite.")
    return dividende / diviseur
