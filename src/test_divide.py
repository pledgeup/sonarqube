import unittest
from divide import divide_by_zero
from divide import division

class TestDivision(unittest.TestCase):
    def test_divide_positive_numbers(self):
        self.assertEqual(division(10, 2), 5.0)

    def test_divide_by_zero(self):
        with self.assertRaises(ZeroDivisionError):
            division(10, 0)

    def test_divide_by_zero(self):
        try:
            result = divide_by_zero()
        except ZeroDivisionError:
            result = None
        self.assertIsNone(result)

if __name__ == '__main__':
    unittest.main()
